package dagistan.sample.scheduledtask;

import java.util.Timer;

public class SchedulerMain {

	public static void main(String args[]) throws InterruptedException {

		Timer time = new Timer(); // Instantiate Timer Object
		ScheduledTask st = new ScheduledTask(); // Instantiate SheduledTask
												// class
		time.schedule(st, 0, 5000); // Create Repetitively task for every 5 secs

		// for demo only.
		int i = 0;
		while (true) {
			i++;
			System.out.println("Execution in Main Thread...." + i);
			Thread.sleep(10000); // waits 10 seconds. as a result we must 3 sysouts in 10 seconds ;)
		}
	}
}